import os

class Config(object):
    # Flask configs
    SECRET_KEY = os.environ.get('SECRET_KEY') or '713secret'
