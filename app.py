from os import listdir, mkdir, remove
from os.path import abspath, isfile, join, splitext
from shutil import copyfile, rmtree

from flask import Flask, render_template, request, redirect, url_for

from flask_bootstrap import Bootstrap
from flask_nav import Nav
from flask_nav.elements import Navbar, View

from flask_wtf import FlaskForm
from wtforms import TextField, SelectField, IntegerField, SubmitField
from wtforms.validators import DataRequired, InputRequired

from pyfluidutils.pyfluidutils import PyFluidLive
from pyfluidsynth3.fluiderror import FluidError

from config import Config

app = Flask(__name__)

app.config.from_object(Config)
nav = Nav()

@nav.navigation()
def mynavbar():
    return Navbar('PiSynth',    View('Home', 'index'),
                                View('Settings', 'settings_view'),
                                View('Connections', 'connections_view'),
                                View('Patch Console', 'patch_console_view')
                            )


@app.route('/')
@app.route('/home')
def index():
    return render_template('home.html', title='FluidWeb', splash_word=app.splash_word)


class SettingForm(FlaskForm):
    key = SelectField('Key', validators=[DataRequired()])
    value = TextField('Value')
    submit = SubmitField('Set')
    get = SubmitField('Get')

class ClearLogForm(FlaskForm):
    submit = SubmitField('Clear Log')

@app.route('/settings', methods=['GET'])
def settings_view():
    setting = SettingForm()
    setting.key.choices = [(k, k) for k in list(app.synth.config)]

    clear_log = ClearLogForm()

    if request.args.get('key') is not None:
        setting.key.default = request.args['key']

    if request.args.get('value') is not None:
        setting.value.default = (request.args['value'])

    setting.process()

    return render_template('settings.html', title='Settings', setting_form=setting, clear_log_form=clear_log, settings_log=reversed(app.settings_log))

@app.route('/set_setting', methods=['POST'])
def set_setting():
    setting = SettingForm()

    if request.method == 'POST':
        key = setting.key.data
        if setting.submit.data:
            value = setting.value.data
            app.synth.settings[key] = value
            app.settings_log.append(key + ' ' + value)
        else:
            value = app.synth.settings[key]

    return redirect('settings?' + '&'.join(["key="+key, "value="+str(value)]))

@app.route('/clear_settings_log', methods=['POST'])
def clear_settings_log():
    clear_log = ClearLogForm()

    if clear_log.validate_on_submit():
        app.settings_log = []

    return redirect(url_for('settings_view'))

class DisconnectForm(FlaskForm):
    submit = SubmitField('Disconnect')

class ConnectionForm(FlaskForm):
    in_client = SelectField('In')
    out_client = SelectField('Out')
    submit = SubmitField('Connect')

@app.route('/connections', methods=['GET'])
def connections_view():
    disconnection = DisconnectForm()
    connection = ConnectionForm()
    connection.in_client.choices = app.synth.search_midi_clients(direction='i')
    connection.out_client.choices = app.synth.search_midi_clients(direction='o')

    return render_template('connections.html', title='Connections', disconnection_form=disconnection, connection_form=connection)

@app.route('/disconnect', methods=['POST'])
def disconnect():
    disconnection = DisconnectForm()

    if request.method == 'POST':
        app.synth.disconnect_midi_clients()

    return redirect(url_for('connections_view'))

@app.route('/connect', methods=['POST'])
def connect():
    connection = ConnectionForm()

    if request.method == 'POST':
        app.synth.connect_midi_clients(connection.in_client.data, connection.out_client.data)

    return redirect(url_for('connections_view'))



class PatchForm(FlaskForm):
    channel = SelectField('Channel', coerce=int)
    soundfont = SelectField('Soundfont', choices=[('', "Found no soundfont in directory")])
    bank = IntegerField('Bank', default=0)
    prog = IntegerField('Instr', default=0)
    refresh = SubmitField('Refresh Filters')
    submit = SubmitField('Program')

@app.route('/patch_console', methods=['GET'])
def patch_console_view():
    patch = PatchForm()
    patch.channel.choices = [(i,i) for i in range(app.synth.settings['synth.midi-channels'])]
    sf = ''
    preset_table = ''

    if request.args.get('chan') is not None:
        patch.channel.default=int(request.args['chan'])

    patch.soundfont.choices = [(sf, sf) for (sf, loaded) in app.sf_list]
    if request.args.get('sf') is not None:
        sf = request.args['sf']
        patch.soundfont.default=sf
        preset_table = splitext(join('preset_tables', sf))[0] + '.html'

    if request.args.get('bank') is not None:
        patch.bank.default=int(request.args['bank'])

    if request.args.get('prog') is not None:
        patch.prog.default=int(request.args['prog'])

    print(app.patch_list)

    patch.process()

    return render_template('patch.html', title='Patch Console', patch_form=patch, patch_list=app.patch_list, soundfont=sf, preset_table=preset_table)

@app.route('/patch', methods=['POST'])
def patch():
    patch = PatchForm()

    if request.method == 'POST':
        chan = patch.channel.data
        sf = patch.soundfont.data
        bank = patch.bank.data
        prog = patch.prog.data
        if patch.submit.data:
            try:
                app.synth.synth.load_soundfont(join(app.sf_dir, sf), reload_presets=False)
            except FluidError:
                print("Error when loading soundfont " + sf)

            app.synth.synth.bank_select(chan, bank)
            app.synth.synth.program_change(chan, prog)
            app.patch_list[chan] = (sf, "{:0>3}-{:0>3}".format(bank, prog))

        chan = 'chan='+str(chan)
        sf = 'sf='+str(sf)
        bank = 'bank='+str(bank)
        prog = 'prog='+str(prog)
        return redirect('/patch_console?' + '&'.join([chan, sf, bank, prog]))

    return redirect(url_for('patch_console_view'))

if __name__ == '__main__':
    app.splash_word = "Floccus"

    Bootstrap(app)
    nav.init_app(app)

    # Settings initializations
    app.settings_list = []
    app.settings_log = []

    # Soundfonts initializations
    app.sf_dir = '../soundfonts'
    app.sf_list = []
    rmtree(join('templates', 'preset_tables'), ignore_errors=True)
    try:
        mkdir(join('templates', 'preset_tables'))
    except OSError:
        pass
    for f in listdir(app.sf_dir):
        if (isfile(join(app.sf_dir, f)) and splitext(join(app.sf_dir, f))[-1].lower() == '.sf2'):
            app.sf_list.append( (f, False) )
            sf_table = splitext(f)[0] + '.html'
            if isfile(join(app.sf_dir, sf_table)):
                copyfile(join(app.sf_dir, sf_table), join(join('templates', 'preset_tables'), sf_table))


    app.synth = PyFluidLive(sf_dir=app.sf_dir, confs_fname='../pyfluidutils/synth')

    app.patch_list = dict()

    app.run(host='0.0.0.0')
